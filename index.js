const express = require('express');
const app = express();
const port = 8000;
const cors = require('cors');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const secret = 'TOP_SECRET';


app.use(cors());

app.use((req, res, next) =>{
    
    if(req.url === "/users/login"){
        next();
    }else{
        const auth = req.headers.authorization;
        jwt.verify(auth, secret, function(err, decoded){
            if(err){
                res.sendStatus(401);
            }else {
                next();
            }
        });
    }
});

app.use(express.json());
app.use(express.urlencoded({ extended: true }));



mongoose.connect('mongodb+srv://uplift-user:uplift@uplift-cluster.6wxlf.mongodb.net/express-training?retryWrites=true&w=majority', {});

// Cannot /GET
// http://localhost:8000/ [GET]
// CRUD functionalities
app.get('/', (req, res) => {
    res.send("Welcome");
});

// <baseurl>/resource
// localhost:8000/users
const UserRouter = require('./routes/users');
app.use('/users', UserRouter);
// localhost:8000/users/
// localhost:8000/users/:id

const ProductRouter = require('./routes/products');
app.use('/products', ProductRouter);

const OrderRouter = require('./routes/orders');
app.use('/orders', OrderRouter);

app.all('*', (req, res) => {
    res.send('Route not found!');
})

app.listen( port, () => { console.log(`Server running at port ${port}`); })