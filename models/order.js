const mongoose = require('mongoose');

const OrderSchema = mongoose.Schema({
    userId: String, 
    name: String,
    date: { 
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Order', OrderSchema);