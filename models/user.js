const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const UserSchema = mongoose.Schema({
    name: String, 
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    created : {
        type: Date,
        default: Date.now
    },
    role: { type: String, enum: ['Admin', 'Student'] },
    validated: {
        type: Boolean,
        default: false
    },
    updated: {
        type: Date,
        default: Date.now
    }
});

UserSchema.methods.validateUser = function validateUser(){
    this.validated = true;
}

UserSchema.methods.isValidPassword = async function isValidPassword(password){
    const result = await bcrypt.compare(password, this.password);

    return result;
}

UserSchema.methods.getOrders = async function getOrders(){
    return mongoose.model("Order").find({ userId: this._id });
}

UserSchema.pre('save', async function(next) {
    const hash = await bcrypt.hash(this.password, 10);
    this.password = hash;
    next();
});

UserSchema.post('save', async function() {
    console.log(this);
});

UserSchema.pre('update', async function(next) {
    this.updated = Date.now
    next();
});

UserSchema.post('update', async function() {
    console.log(this);
});




module.exports = mongoose.model('User', UserSchema);