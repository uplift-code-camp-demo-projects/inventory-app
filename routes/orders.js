const router = require('express').Router();
const OrderModel = require('../models/order');

router.post('/', (req, res) =>{
    const order = new OrderModel(req.body);
    order.save().then(order =>{
        res.status(201).send(order);
    });
})


module.exports = router;