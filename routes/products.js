const router = require('express').Router();
const ProductModel = require('../models/product');

router.get('/', async (req, res) => {   
    const products = await ProductModel.find();

    res.send(products);
});

router.get('/query/:limit?', async (req, res) => {   
    const products = await ProductModel
    .find()
    .where("stock").gte(req.params.limit)
    .where("name").in(['em', 'michiel'])
    .select("name stock")
    .sort("-stock")
    .exec();

    res.send(products);
});

router.get('/:id', async (req, res) => {
    // req.params.id   
    const products = await ProductModel
    .find({ _id : req.params.id })
    .sort("name")
    res.send(products);
});


router.post('/', (req,res) => {
    const product = new ProductModel(req.body);
    product.save().then(user =>{
        res.status(201).send(user);
    });
});

router.put('/:id', (req, res) => {

    ProductModel.findOneAndUpdate(
        { _id: req.params.id }, 
        req.body,
        { new: true })
        .then(data => {
            res.status(200).send(data);
        })
        .catch(err => {
            res.status(500).send(err);
        })
})
module.exports = router;