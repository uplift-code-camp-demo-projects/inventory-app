const router = require('express').Router();
const UserModel = require('../models/user');
const jwt = require('jsonwebtoken');
const secret = 'TOP_SECRET';
// router.use((req, res, next) =>{
//     if(req.method === 'POST'){
//         next();
//     }else {
//         res.sendStatus(401);
//     }
// });

// router.use('/', (req, res, next) => {
//     if(req.method === 'POST'){
//         if(!req.body.name) res.send(400);
//         if(!req.body.role) res.send(400);
//         next();
//     }else {
//         next();
//     }
// });

router.get('/', (req, res) => {   
    UserModel.find().then( response => {
        res.send(response);
    })
});

router.get('/:id', (req, res) => {
    // req.params.id   
    UserModel.find({ _id : req.params.id }).then( response => {
        res.send(response);
    })
});

router.get('/:id/orders',(req, res) => {
    UserModel.findOne({ _id : req.params.id }).then( user => {
        return user.getOrders();
    })
    .then(orders => {
        res.send(orders);
    })
})

router.post('/', (req,res) => {
    const user = new UserModel(req.body);
    user.save().then(user =>{
        res.status(201).send(user);
    })
    .catch(err => {
        res.status(500).send(err);
    })
});

router.post('/login', (req, res) => {
    UserModel.findOne({ email : req.body.email })
        .then(async (user) =>{
            const valid = await user?.isValidPassword(req.body.password);
            if(valid){
                const token = jwt.sign({ ...user._doc}, secret, {
                    expiresIn: "20d"
                });
                res.json({ token })
            }else {
                res.sendStatus(401);
            }      
        });
});

router.put('/:id', (req, res) => {
    UserModel.findOneAndUpdate(
        { _id: req.params.id }, 
        req.body,
        { new: true })
        .then(data => {
            res.status(200).send(data);
        })
        .catch(err => {
            res.status(500).send(err);
        })
});

router.get('/validate/:id', (req, res, next) => {
  
    UserModel.findOne({ _id : req.params.id })
    .then( user => {
        user.validateUser();
        return user.save();
    })
    .then(updatedUser => {
        res.send(updatedUser);
    })
    .catch(err => {
        next(err);
    });
});

router.delete('/:id', (req, res) => {
    UserModel.findOneAndDelete({ _id: req.params.id })
        .then(data =>{
            console.log(data);
            res.status(200).send(data);
        });
})

module.exports = router;